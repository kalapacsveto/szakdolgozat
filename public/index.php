<?php

  date_default_timezone_set('Europe/Budapest');

  ini_set('display_errors', 0);
  error_reporting(0);

  if(DEBUG_MODE){
      ini_set('display_errors', 1);
      error_reporting(E_ALL);
    }

  require '../config.php';
  require '../vendor/autoload.php';

  use Library\Core;

  try {
    $core = new Core();
  } catch (\Exception $e) {
    if(DEBUG_MODE){
      echo $e->getMessage();
    }else{
      // TODO: ha nem DEBUG, akkor error reportot kellene küldeni valahova, nem a kimenetre küldeni
    }

  }


?>
