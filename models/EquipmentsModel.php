<?php
namespace Models;
use Models\Equipments_sportsModel;

class EquipmentsModel extends ModelBase
{
  const TABLE_NAME = 'equipments';

  public function getEquipments($waste){

    switch ($waste) {
      case true:
        $query = 'SELECT * FROM '.self::TABLE_NAME.' ORDER BY name ASC';
        break;

      default:
        $query = 'SELECT * FROM '.self::TABLE_NAME.' WHERE waste = 0 ORDER BY name ASC';
        break;
    }
    $equipments = $this->findAll($query);
    foreach($equipments as &$equipment){
      $equipment['sports'] = Equipments_sportsModel::get()->getEquipment_sportsById($equipment['id']);
    }
    return $equipments;
  }

  public function getEquipmentById($id){
      return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE id = :id', ['id' => $id]);
  }

  public function saveEquipment($params){
    $data = array();
    $return = array(
      'status' => false
    );
    if(!array_key_exists('name', $params)){
      return $return;
    }

    if( mb_strlen($_POST['name']) < 4 ){
      return $return;
    }

    $data['name'] = $_POST['name'];
    try {
      $this->prepareQuery('INSERT INTO '.self::TABLE_NAME.' (name) VALUES (:name)', $data);
    } catch (\PDOException $e) {
      $return['error_message'] = $e->getMessage();
      return $return;
    }

    if (!$this->getLastInsertedId()) {
        return $return;
    }

    $return['status'] = true;
    $return['id'] = $this->getLastInsertedId();
    return $return;
  }

  public function updateEquipment($ID, $params){
    $return = array('status' => false);
    $keys = array('name', 'waste', 'properties');
    $data = array(
      'id' => (int)$ID
    );
    $set = array();
    try {
      foreach($params as $param => $value){
        if(in_array($param, $keys)){
          $data[$param] = $value;
          $set[] = $param.'=:'.$param;
        }
      }

    $update = $this->prepareQuery('UPDATE '.self::TABLE_NAME.' SET '.implode(', ', $set).' WHERE id=:id', $data );
    } catch(\PDOException $e){
      $return['error_message'] = $e->getMessage();
      return $return;
    }

    $return['status'] = true;
    $return['row_count'] = $update->rowCount();

    return $return;
;
  }


  static function get()
  {
      $class = get_class();
      return new $class;
  }
}

?>
