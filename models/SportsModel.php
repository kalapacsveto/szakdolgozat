<?php
namespace Models;

class SportsModel extends ModelBase
{
  const TABLE_NAME = 'sports';

  public function getSports(){
      return $this->findAll('SELECT * FROM '.self::TABLE_NAME.' ORDER BY name ASC');
  }

  public function getSportById($id){
      return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE id = :id', ['id' => $id]);
  }

  static function get()
  {
      $class = get_class();
      return new $class;
  }
}

?>
