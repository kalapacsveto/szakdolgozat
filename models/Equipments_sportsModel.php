<?php
namespace Models;

class Equipments_sportsModel extends ModelBase
{
  const TABLE_NAME = 'equipments_sports';
  const SPORTS_TABLE_NAME = 'sports';

  public function getEquipment_sportsById($equipment_id){
      return $this->findAll(
        "SELECT `".self::SPORTS_TABLE_NAME."`.`id` AS 'sport_id', `".self::SPORTS_TABLE_NAME."`.`name`, `".self::TABLE_NAME."`.`id` AS 'equipments_sports_id'
        FROM `".self::TABLE_NAME."`
        INNER JOIN `".self::SPORTS_TABLE_NAME."`
        ON `".self::SPORTS_TABLE_NAME."`.`id` = `".self::TABLE_NAME."`.`sport_id`
        WHERE ".self::TABLE_NAME.".equipment_id = :equipment_id  ORDER BY sport_id ASC", ['equipment_id' => $equipment_id]
      );
  }

  public function saveEquipmentSport($equipment_id, $sport_id){

    try {
      $this->prepareQuery('INSERT INTO '.self::TABLE_NAME.' (equipment_id, sport_id) VALUES (:equipment_id,:sport_id)', array('equipment_id' => $equipment_id, 'sport_id' => $sport_id));

    } catch (\PDOException $e) {
      var_dump($e->getMessage());
    }

    if (!$this->getLastInsertedId()) {
        return false;
    }
    else{
      return $this->getLastInsertedId();
    }
  }

  public function flushEquipmentSport($equipment_id){
    $this->prepareQuery('DELETE FROM '.self::TABLE_NAME.' WHERE equipment_id = :equipment_id', array('equipment_id' => $equipment_id));
  }

  static function get()
  {
      $class = get_class();
      return new $class;
  }
}

?>
