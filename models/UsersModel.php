<?php
namespace Models;
use Models\Users_sportsModel;

class UsersModel extends ModelBase
{
  const TABLE_NAME = 'users';

  public function getUserByEmail($email){
      return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE email = :email', ['email' => $email]);
  }

  public function getUsers(){
      $users = $this->findAll('SELECT * FROM '.self::TABLE_NAME.' ORDER BY full_name ASC');

      foreach($users as &$user){
        $user['sports'] = Users_sportsModel::get()->getUsers_sportsById($user['id']);
      }

      return $users;
  }

  public function getUserById($id){
      return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE id = :id', ['id' => $id]);
  }

  static function get()
  {
      $class = get_class();
      return new $class;
  }
}

?>
