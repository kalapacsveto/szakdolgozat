<?php
namespace Models;

class Users_sportsModel extends ModelBase
{
  const TABLE_NAME = 'users_sports';
  const SPORTS_TABLE_NAME = 'sports';

  public function getUsers_sportsById($user_id){
      return $this->findAll(
        "SELECT `".self::SPORTS_TABLE_NAME."`.`id` AS 'sport_id', `".self::SPORTS_TABLE_NAME."`.`name`, `".self::TABLE_NAME."`.`id` AS 'user_sports_id'
        FROM `".self::TABLE_NAME."`
        INNER JOIN `".self::SPORTS_TABLE_NAME."`
        ON `".self::SPORTS_TABLE_NAME."`.`id` = `".self::TABLE_NAME."`.`sport_id`
        WHERE ".self::TABLE_NAME.".user_id = :user_id  ORDER BY sport_id ASC", ['user_id' => $user_id]
      );
  }


  static function get()
  {
      $class = get_class();
      return new $class;
  }
}

?>
