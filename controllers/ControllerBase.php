<?php
namespace Controllers;

abstract class ControllerBase
{
    protected $currentLayout = 'main';
    private $currentAction = 'index';
    protected $viewDir = '../views';
    protected $publicDir = '../public';
    protected $viewFile = '';
    public $content;
    public $js = array('index.js');
    public $title = '';
    protected $disableLayout = false;

    public function __construct()
      {
        $reflect = new \ReflectionClass($this);
        $this->controllerName = lcfirst(str_replace('Controller', '', $reflect->getShortName()));
        if (!isset($_SESSION['user'])) {
          if($this->controllerName !== 'login'){
            $this->redirect('login');
          }
        }
      }

      protected function js_list()
      {
        $return = '';
          foreach($this->js as $js){
            $return.='<script src="/js/'. $js .'"></script>';
          }
        echo $return;
      }

      protected function redirect($url)
      {
        if (headers_sent()){
          foreach (headers_list() as $header){
            header_remove($header);
            }
          }
        header("Location: $url");
        die();
      }

      protected function render($view, $params = [])
      {
        if (file_exists($this->viewFile)){
            $this->setViewContent();
            if (!$this->disableLayout){
                include($this->viewDir.'/layouts/' . $this->currentLayout . '.php');
            }
            return $this->content;
        } else {
          throw new \Exception('view file not exists');
        }
      }

      protected function json_response($response_data = false, $status_code = 200){
        if (!headers_sent()) {
            header_remove();
          }
        header('Content-Type: application/json');

        switch ($status_code) {
          case 200:
            $message = 'OK';
          break;
          case 400:
            $message = 'Bad Request';
            break;
          case 401:
            $message = 'Unauthorized';
            break;
          case 404:
            $message = 'Not Found';
            break;
          case 405:
            $message = 'Method Not Allowed';
            break;
          default:
            $status_code = 500;
            $message = 'Internal Server Error';
        }
        header("HTTP/1.0 ".$status_code." ".$message);

        if(is_array($response_data)){
          echo json_encode($response_data);
        }
        else{
          json_encode(array('message' => $message));
        }

      }

      private function setViewContent()
      {
          ob_start();
          include $this->viewFile;
          $this->content = ob_get_contents();
          ob_end_clean();
      }

      public function actionIndex(){
        $view = 'index';
        $this->viewFile = $this->viewDir .'/'. $this->controllerName . '/' . $view . '.php';
        $this->render($view);
      }
}

?>
