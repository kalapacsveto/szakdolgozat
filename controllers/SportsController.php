<?php
namespace Controllers;
use Models\SportsModel;

class SportsController extends ControllerBase{
  public $title = 'Sportok';

  public function actionList(){
    $sports = SportsModel::get()->getSports();
    $this->json_response(array('data'=> $sports));
  }
}
?>
