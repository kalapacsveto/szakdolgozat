<?php
namespace Controllers;
use Models\EquipmentsModel;
use Models\Equipments_sportsModel;

class EquipmentsController extends ControllerBase{
  public $title = 'Eszközök';

  public function actionIndex(){

    $this->currentLayout = 'main';
    $this->js[] = 'equipments.js';
    $view = 'index';
    $this->viewFile = $this->viewDir .'/equipments/' . $view . '.php';
    $this->render($view);
  }

  public function actionList(){
    if($_SERVER['REQUEST_METHOD'] !== 'GET'){
      $this->json_response(array('message'=> 'Method Not Allowed'), 405);
      return;
    }
    $waste = false;
    if(array_key_exists('waste', $_GET)){
      $waste = true;
    }
    $equipments = EquipmentsModel::get()->getEquipments($waste);
    $this->json_response(array('data'=> $equipments));
  }

  public function actionAdd(){
    $statuscode = 200;
    if($_SERVER['REQUEST_METHOD'] !== 'POST'){
      $this->json_response(array('message'=> 'Method Not Allowed'), 405);
      return;
    }
    $equipment = EquipmentsModel::get()->saveEquipment(array('name' => $_POST['name']));
    if(!$equipment['status']){
      $statuscode = 400;
    } else {
      if(array_key_exists('sports',$_POST)){
        $equipment['sports'] = $this->addSports($equipment['id'], $_POST['sports']);
      }
    }
    $this->json_response($equipment, $statuscode);
  }

  public function actionUpdate(){
    $updated = [];
    $equipmentUpdateData = array();
    if($_SERVER['REQUEST_METHOD'] !== 'POST'){
      $this->json_response(array('message'=> 'Method Not Allowed'), 405);
      return;
    }

  if(array_key_exists('name',$_POST)){
    $equipmentUpdateData['name'] = $_POST['name'];
  }

  if(array_key_exists('waste',$_POST)){
    $equipmentUpdateData['waste'] = (int)$_POST['waste'];
  }

  if(count($equipmentUpdateData)){
    $equipment_updata = EquipmentsModel::get()->updateEquipment( (int)$_POST['id'], $equipmentUpdateData );
    $updated[] = 'equipment';
  }

  if(array_key_exists('sports',$_POST)){
    $updated[] = 'equipment_sports';
    Equipments_sportsModel::get()->flushEquipmentSport((int)$_POST['id']);
    if(is_array($_POST['sports'])){
      $this->addSports( (int)$_POST['id'], $_POST['sports']);
    }
  }

  $this->json_response(array('updated' => $updated), 200);
  }

  public function actionData(){
    if($_SERVER['REQUEST_METHOD'] !== 'GET'){
      $this->json_response(array('message'=> 'Method Not Allowed'), 405);
      return;
    }
    $equipment = EquipmentsModel::get()->getEquipmentById((int)$_GET['id']);
    if(!$equipment){
      $this->json_response(array(), 404);
    }

    $sports = Equipments_sportsModel::get()->getEquipment_sportsById((int)$_GET['id']);
    $this->json_response(array('data'=> $equipment, 'sports' => $sports));
  }

  private function addSports($equipment_id, $Sports){
    $SavedIDs = array();
    if(is_string($Sports)){
      $Sports = array($Sports);
    }

    foreach($Sports as $Sport){

      $save = Equipments_sportsModel::get()->saveEquipmentSport($equipment_id, (int)$Sport);
      if($save){
        $SavedIDs[] = $save;
      }
    }

    return $SavedIDs;
  }
}
?>
