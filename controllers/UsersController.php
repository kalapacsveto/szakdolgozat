<?php
namespace Controllers;
use Models\UsersModel;

class UsersController extends ControllerBase{
  public $title = 'Felhasználók';

  public function actionIndex(){
    if($_SESSION['user']['level'] == 'admin'){
      $this->title = 'Felhasználók listája';
    }
    else{
      $this->title = 'Felhasználó adatai';
    }
    $this->currentLayout = 'main';
    $this->js[] = 'users_'.$_SESSION['user']['level'].'.js';
    $view = 'index';
    $this->viewFile = $this->viewDir .'/users/' . $view . '_'.$_SESSION['user']['level'].'.php';
    $this->render($view);
  }

  public function actionList(){
    if($_SERVER['REQUEST_METHOD'] !== 'GET'){
      $this->json_response(array('message'=> 'Method Not Allowed'), 405);
      return;
    }

    $users = UsersModel::get()->getUsers();
    $this->json_response(array('data'=> $users));
  }


}

?>
