<?php
namespace Controllers;
use Models\UsersModel;

class LoginController extends ControllerBase{
  public $title = 'Bejelentkezés';

  public function actionIndex(){
    if (isset($_SESSION['user'])){
      $this->redirect('/');
    }
    $this->currentLayout = 'main_guest';
    $this->js[] = 'login.js';
    $view = 'login';
    $this->viewFile = $this->viewDir .'/home/' . $view . '.php';
    $this->render($view);
  }

  public function actionLogout(){
    if (!isset($_SESSION['user'])){
      $this->redirect('login');
    }else{
      session_unset();
      $this->redirect('/');
    }

  }

  public function actionRequest(){
    // password create: password_hash($_POST['password'], PASSWORD_DEFAULT);
    if($_SERVER['REQUEST_METHOD'] !== 'POST'){
      $this->json_response(array('message'=> 'Method Not Allowed'), 405);
      return;
    }

    if(array_key_exists('email',$_POST) && array_key_exists('password',$_POST)){

      try {

        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
          throw new \Exception("email validation error");
        }
        $user = UsersModel::get()->getUserByEmail($_POST['email']);
        if(!$user){
          throw new \Exception("get user error");
        }

        if(password_verify($_POST['password'],$user['password'])){
          $_SESSION['user'] = $user;
        }else{
          throw new \Exception("password error");
        }

      } catch (\Exception $e) {
        $this->json_response(array('message'=> 'Unauthorized'), 401);
        return;
      }


    }
    else{
      $this->json_response(array('message'=> 'Unauthorized'), 401);
      return;
    }

  }


}
?>
