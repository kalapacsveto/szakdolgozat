<?php
namespace Library;

class Core
{
  private $currentController = 'home';
  private $currentAction = 'index';
  private $controllerDir = 'Controllers';
  private $requestUrlData = [];

  public function __construct(){
      if ( session_status() !== PHP_SESSION_ACTIVE) {
          session_start();
          $this->app_start();
      }
  }


  public function __toString(){
    echo '$currentController: '.$this->currentController.'\n';
    echo '$currentAction: '.$this->currentAction.'\n';
    echo '$requestUrlData:\n';
    foreach($this->requestUrlData as $requestUrlData){
      echo $requestUrlData.' ';
    }
  }

  private function app_start(){
    $this->request_url_process();
    $controllerName = '\\'.$this->controllerDir.'\\'.ucfirst($this->currentController).'Controller';
    if (!class_exists($controllerName)){
      throw new \Exception('Controller not exists');
    }
    $controller = new $controllerName();
    $action = 'action'.ucfirst($this->currentAction);
    if (method_exists($controller, $action)) {
        return $controller->$action($this->getUrlArg(3));
    } else {
      $headers = array();
      if (!headers_sent()) {
          $headers = headers_list();
          header_remove();
        }
      ob_clean();

      header("HTTP/1.0 404 Not Found");
      if(in_array('Content-Type: application/json', $headers)){
        header('Content-Type: application/json');
        echo json_encode(array('message' => 'not found'));
      }
      else{
        echo '404';
      }
    }

  }

  private function request_url_process(){
    $request_uri = ltrim($_SERVER['REQUEST_URI'],'/');
    $this->requestUrlData = explode('/',explode('?',$request_uri)[0]);
    if($this->requestUrlData[0] !== ''){
      $this->currentController = $this->requestUrlData[0];
    }
    if(count($this->requestUrlData) >= 2){
      $this->currentAction = $this->requestUrlData[1];
    }
  }

  private function getUrlArg($index){
    if(!is_int($index)){
      throw new \Exception('getUrlArg index is not integer');
    }
    $index--;

    return isset($this->requestUrlData[$index]) ? $this->requestUrlData[$index] : null;
  }

}
?>
