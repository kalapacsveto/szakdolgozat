const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'production',
    entry : {
      index : './src/js/index.js',
      login : './src/js/login.js',
      users_admin : './src/js/users_admin.js',
      equipments : './src/js/equipments.js'
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, "public/js"),
    },
    target: 'web',
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        }
      ]
    },
    plugins : [
      new CleanWebpackPlugin(),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        _ : 'underscore'
      })
    ]
}
