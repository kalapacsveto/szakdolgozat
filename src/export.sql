-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Sze 28. 17:52
-- Kiszolgáló verziója: 10.3.17-MariaDB-0+deb10u1
-- PHP verzió: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `sportszerny`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `equipments`
--

CREATE TABLE `equipments` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `properties` varchar(1024) NOT NULL,
  `waste` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- A tábla adatainak kiíratása `equipments`
--

INSERT INTO `equipments` (`id`, `name`, `properties`, `waste`) VALUES
(1, 'Adidasz susogós melegítő', '', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `equipments_rent`
--

CREATE TABLE `equipments_rent` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `equipments_id` int(11) NOT NULL,
  `comment` varchar(512) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `equipments_rent`
--

INSERT INTO `equipments_rent` (`id`, `users_id`, `equipments_id`, `comment`, `updated`) VALUES
(3, 2, 1, 'tesztre kiadva', '2020-07-20 18:05:10'),
(4, 2, 1, 'visszahozta tisztán', '2020-07-20 19:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `equipments_sports`
--

CREATE TABLE `equipments_sports` (
  `id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `sport_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` enum('paid','unpaid') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `membership_fee`
--

CREATE TABLE `membership_fee` (
  `id` int(11) NOT NULL,
  `updated` date NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sports`
--

CREATE TABLE `sports` (
  `id` int(11) NOT NULL,
  `name` varchar(512) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `properties` varchar(1024) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `sports`
--

INSERT INTO `sports` (`id`, `name`, `properties`) VALUES
(1, 'kalapácsvetés', ''),
(2, 'diszkoszvetés', ''),
(3, 'súlylökés', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(512) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `full_name` varchar(512) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `date_of_brith` date NOT NULL,
  `password` varchar(512) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `level` enum('user','admin') NOT NULL DEFAULT 'user',
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `email`, `full_name`, `date_of_brith`, `password`, `level`, `status`) VALUES
(2, 'kalapacsveto01@gmail.com', 'Gál Éva', '1990-01-01', '', 'admin', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users_sports`
--

CREATE TABLE `users_sports` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sport_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `users_sports`
--

INSERT INTO `users_sports` (`id`, `user_id`, `sport_id`) VALUES
(2, 2, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users_sport_types`
--

CREATE TABLE `users_sport_types` (
  `id` int(11) NOT NULL,
  `users_sports_id` int(11) NOT NULL,
  `users_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users_type`
--

CREATE TABLE `users_type` (
  `id` int(11) NOT NULL,
  `type` enum('trainer','athlete') CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `users_type`
--

INSERT INTO `users_type` (`id`, `type`) VALUES
(1, 'trainer'),
(2, 'athlete');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `equipments`
--
ALTER TABLE `equipments`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `equipments_rent`
--
ALTER TABLE `equipments_rent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipments_rent_users` (`users_id`),
  ADD KEY `equipments_rent_equipments` (`equipments_id`);

--
-- A tábla indexei `equipments_sports`
--
ALTER TABLE `equipments_sports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipments_fgk` (`equipment_id`),
  ADD KEY `sports_fgk` (`sport_id`);

--
-- A tábla indexei `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership_users` (`users_id`);

--
-- A tábla indexei `membership_fee`
--
ALTER TABLE `membership_fee`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `level` (`level`);

--
-- A tábla indexei `users_sports`
--
ALTER TABLE `users_sports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_sports_sports_fgk` (`sport_id`),
  ADD KEY `user_sports_users_fgk` (`user_id`);

--
-- A tábla indexei `users_sport_types`
--
ALTER TABLE `users_sport_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_sport_types_sports_fgk` (`users_sports_id`),
  ADD KEY `users_sport_types_type_fgk` (`users_type_id`);

--
-- A tábla indexei `users_type`
--
ALTER TABLE `users_type`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `equipments`
--
ALTER TABLE `equipments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `equipments_rent`
--
ALTER TABLE `equipments_rent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `equipments_sports`
--
ALTER TABLE `equipments_sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `membership_fee`
--
ALTER TABLE `membership_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `sports`
--
ALTER TABLE `sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `users_sports`
--
ALTER TABLE `users_sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `users_sport_types`
--
ALTER TABLE `users_sport_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `users_type`
--
ALTER TABLE `users_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `equipments_rent`
--
ALTER TABLE `equipments_rent`
  ADD CONSTRAINT `equipments_rent_equipments` FOREIGN KEY (`equipments_id`) REFERENCES `equipments` (`id`),
  ADD CONSTRAINT `equipments_rent_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Megkötések a táblához `equipments_sports`
--
ALTER TABLE `equipments_sports`
  ADD CONSTRAINT `equipments_fgk` FOREIGN KEY (`equipment_id`) REFERENCES `equipments` (`id`),
  ADD CONSTRAINT `sports_fgk` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`id`);

--
-- Megkötések a táblához `membership`
--
ALTER TABLE `membership`
  ADD CONSTRAINT `membership_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Megkötések a táblához `users_sports`
--
ALTER TABLE `users_sports`
  ADD CONSTRAINT `user_sports_users_fgk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_sports_sports_fgk` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`id`);

--
-- Megkötések a táblához `users_sport_types`
--
ALTER TABLE `users_sport_types`
  ADD CONSTRAINT `users_sport_types_sports_fgk` FOREIGN KEY (`users_sports_id`) REFERENCES `users_sports` (`id`),
  ADD CONSTRAINT `users_sport_types_type_fgk` FOREIGN KEY (`users_type_id`) REFERENCES `users_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
