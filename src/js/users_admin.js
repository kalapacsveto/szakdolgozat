import {
  CreateModal
} from './Helper/Modal.js';

import {
  SportsGroup
} from './Helper/Form.js';

import {
  GetSportsList,
  GetUsers
} from './Helper/Request.js';

let Selectors = {
  ReloadButton : $('#users_reload'),
  UsersTable : $('#users_list'),
  UserAdd : $('#users_add')
}

let Templates = {
  UsersTableRow : _.template(`<tr>
    <th scope="row"><%-data.id%></th>
    <td><%-data.full_name%></td>
    <td><%-data.email%></td>
    <td><%-data.level%></td>
    <td><%=sports_list%></td>
    <td><div class="btn-group btn-group-sm" role="group"><button type="button" class="btn btn-secondary equipment_editor" data-equipment_id="<%-data.id%>">Módosítás</button></div></td>
    </tr>`),
  EquipmentsTableSports : _.template('<span class="badge badge-dark mr-1"><%-name%></span>')
}

let LoadUsers = function(button){
  let tbody = Selectors.UsersTable.find('tbody');
  tbody.html('');
  button.prop('disabled', true);

GetUsers().done(function(Data){
    _.each(Data.data, function(User, i){

      let sport_badge_templates = [];
      _.each(User.sports, function(sport){
        sport_badge_templates.push(Templates.EquipmentsTableSports(sport));
      });

      let TableRow = $(Templates.UsersTableRow({
        data: User,
        sports_list: sport_badge_templates.join('')
      })).appendTo(tbody);
    });

  });
}

let AddUser = function(button){
  let UserModal = CreateModal('lg', true);
  UserModal.Modal.modal('show');
  UserModal.Title.html('Felhasználó felvétele');

  GetSportsList().done(function(Data){
    UserModal.Body.html('');

    let Form = CreateUserForm( UserModal.Body, Data.data );
  });

}

let CreateUserForm = function(Target, Sports){
  let Form = $('<form>')
    .appendTo(Target);

    let FullNameGroup = $('<div>')
      .addClass('form-group row')
      .appendTo(Form);

    let EmailGroup = $('<div>')
      .addClass('form-group row')
      .appendTo(Form)

    let PasswordGroup = $('<div>')
      .addClass('form-group row')
      .appendTo(Form)

    let LevelGroup = $('<div>')
      .addClass('form-group row')
      .appendTo(Form)


    SportsGroup( Form, Sports );


    $('<label>')
      .addClass('col-sm-2 col-form-label')
      .attr('for', 'full_name')
      .html('Név')
      .appendTo(FullNameGroup);

    $('<label>')
      .addClass('col-sm-2 col-form-label')
      .attr('for', 'email')
      .html('E-mail cím')
      .appendTo(EmailGroup);

    $('<label>')
      .addClass('col-sm-2 col-form-label')
      .attr('for', 'password')
      .html('Jelszó')
      .appendTo(PasswordGroup);

    $('<label>')
      .addClass('col-sm-2 col-form-label')
      .attr('for', 'level')
      .html('Típus')
      .appendTo(LevelGroup);


    let NameGroupInputDiv = $('<div>')
      .addClass('col-sm-10')
      .appendTo(FullNameGroup);

    let EmailGroupInputDiv = $('<div>')
      .addClass('col-sm-10')
      .appendTo(EmailGroup);

    let PasswordGroupInputDiv = $('<div>')
      .addClass('col-sm-10')
      .appendTo(PasswordGroup);

    let LevelGroupInputDiv = $('<div>')
      .addClass('col-sm-10')
      .appendTo(LevelGroup);


    $('<input>')
      .addClass('form-control')
      .attr('type', 'text')
      .attr('name', 'full_name')
      .attr('placeholder', 'Név, minimum 3 karakter')
      .appendTo(NameGroupInputDiv);

    $('<input>')
      .addClass('form-control')
      .attr('type', 'email')
      .attr('name', 'email')
      .attr('placeholder', 'E-mail cím')
      .appendTo(EmailGroupInputDiv);

    $('<input>')
      .addClass('form-control')
      .attr('type', 'password')
      .attr('name', 'password')
      .attr('placeholder', 'Jelszó')
      .appendTo(PasswordGroupInputDiv);

    let LevelSelect = $('<select>')
      .addClass('custom-select')
      .appendTo(LevelGroupInputDiv);

    $('<option>')
      .html('Adminisztrátor')
      .attr('name', 'level')
      .attr('value', 'admin')
      .appendTo(LevelSelect);

    $('<option>')
      .html('Felhasználó')
      .attr('name', 'level')
      .attr('value', 'user')
      .appendTo(LevelSelect).prop('selected', true);
}

Selectors.ReloadButton.click(function(){ LoadUsers($(this)) });
Selectors.UserAdd.click(function(){ AddUser($(this)) })

Selectors.ReloadButton.trigger('click');
