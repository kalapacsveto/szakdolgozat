import {
  CreateModal
} from './Helper/Modal.js';

import {
  SportsGroup
} from './Helper/Form.js';

import {
  GetSportsList,
  GetEquipmentData,
  UpdateEquipment,
  AddEquipment,
  GetEquipments
} from './Helper/Request.js';

let Selectors = {
  ReloadButton : $('button#equipments_reload'),
  AddButton : $('button#equipments_add'),
  EquipmentsTable : $('table#equipments_list'),
  Filter : {
    Waste : $('input[name="filter_waste"]')
  }
}

let Templates = {
  EquipmentsTableRow : _.template('<tr><th scope="row"><%-data.id%></th><td><%-data.name%></td><td></td><td><%=sports_list%></td><td><div class="btn-group btn-group-sm" role="group"><button type="button" class="btn btn-secondary equipment_editor" data-equipment_id="<%-data.id%>">Módosítás</button></div></td></tr>'),
  EquipmentsTableSports : _.template('<span class="badge badge-dark mr-1"><%-name%></span>')
}

let LoadEquipments = function(button){
  let Filter = {};
  let tbody = Selectors.EquipmentsTable.find('tbody');
  tbody.html('');
  button.prop('disabled', true);

  if(Selectors.Filter.Waste.is(':checked') ){
    Filter.waste = true;
  }

  GetEquipments(Filter).done(function(Data){
    _.each(Data.data, function(Equipment, i){
      let sport_badge_templates = [];
      _.each(Equipment.sports, function(sport){
        sport_badge_templates.push(Templates.EquipmentsTableSports(sport));
      });

      let TableRow = $(Templates.EquipmentsTableRow({
        data: Equipment,
        sports_list: sport_badge_templates.join('')
      })).appendTo(tbody);

      if(Equipment.waste == 1){
        TableRow.addClass('table-danger')
      }

    });
    button.prop('disabled', false);
  }).fail(function(Data){
    console.error(Data);
  });
}

let EquipmentFormData = function(Form){
  let Defaults = {
    'name' : {name : 'name', value: ''},
    'sports[]' : {name : 'sports', value : false}
  };
  let FormData = Form.serializeArray();
  let Keys = _.pluck(FormData, 'name');
  _.each(_.keys(Defaults), function(Key){
    if(_.indexOf(Keys, Key) < 0){
      FormData.push(Defaults[Key]);
    }
  })
  return FormData;
}

let EquipmentEditor = function( Button ){
  let ID = Button.attr('data-equipment_id');
  let EquipmentsModal = CreateModal('lg', true);
  EquipmentsModal.Modal.modal('show');
  EquipmentsModal.Title.html('Eszköz szerkesztése');
  let SportList = GetSportsList();
  SportList.done(function(SportsData){
    let EquipmentData = GetEquipmentData(ID);
    EquipmentData.done(function(EquipmentData){
      EquipmentsModal.Body.html('');
      let Form = CreateEquipmentForm( EquipmentsModal.Body, SportsData.data );
      Form.find(':input[name="name"]').val(EquipmentData.data.name);
      _.each( EquipmentData.sports, function(Sport){
        Form.find(':input[name="sports[]"][value="'+Sport.sport_id+'"]').prop('checked', true);
      });
      let SaveButton = $('<button>')
        .addClass('btn btn-primary')
        .attr('type', 'button')
        .html('Mentés')
        .appendTo(EquipmentsModal.Footer);

      let WasteButton = $('<button>')
        .addClass('btn')
        .attr('type', 'button')
        .attr('data-state', EquipmentData.data.waste)
        .prependTo(EquipmentsModal.Footer);

      UpdateWasteButton(WasteButton, EquipmentData.data.waste);

      WasteButton.click(function(event){
        WasteButton.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        let FormData = [];
        let Button = $(this);
        let CurrentState = Button.attr('data-state');
        Button.prop('disabled', true);
        let NewState;
        switch(CurrentState){
            case '1':
            NewState = 0;
            break;
          default:
            NewState = 1;
        }
        FormData.push({name : 'waste', value : NewState});
        FormData.push({name : 'id', value: ID});

        UpdateEquipment(FormData).done(function(Data){
          Button.prop('disabled', false);
          UpdateWasteButton(WasteButton, NewState);
          Selectors.ReloadButton.trigger('click');
        })

      })

      SaveButton.click(function(event){
        let Button = $(this);
        let FormData = EquipmentFormData(Form);
        Form.find(':input').prop('disabled', true);
        Button.prop('disabled', true);
        FormData.push({name : 'id', value: ID});
        UpdateEquipment(FormData).done(function(Data){
          Selectors.ReloadButton.trigger('click');
          EquipmentsModal.Modal.modal('hide');
        })

      });
    })
  });
}

let UpdateWasteButton = function(WasteButton, State){

  WasteButton.removeClass('btn-success btn-danger');
  WasteButton.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
  switch (State) {
    case '1':
    case 1:
      WasteButton.addClass('btn-success');
      WasteButton.html('Visszaállítás');
      break;
    default:
      WasteButton.addClass('btn-danger');
      WasteButton.html('Törlés');
  }
  WasteButton.attr('data-state', State);
  return WasteButton;
}

let AddEquipments = function(){
  let EquipmentsModal = CreateModal('lg', true);
  EquipmentsModal.Modal.modal('show');
  EquipmentsModal.Title.html('Eszköz hozzáadása');
  GetSportsList().done(function(Data){
    EquipmentsModal.Body.html('');

  let Form = CreateEquipmentForm( EquipmentsModal.Body, Data.data );

  let SaveButton = $('<button>')
    .addClass('btn btn-primary')
    .attr('type', 'button')
    .html('Mentés')
    .appendTo(EquipmentsModal.Footer);

  SaveButton.click(function(event){
    let Button = $(this);
    let FormData = Form.serializeArray();
    Form.find(':input').prop('disabled', true);
    Button.prop('disabled', true);

    AddEquipment(FormData).done(function(Data){
      EquipmentsModal.Modal.modal('hide');
      Selectors.ReloadButton.trigger('click');
    })
    .fail(function(Data){
      Form.find(':input').prop('disabled', false);
      Button.prop('disabled', false);
    });

  })

  });
}

let CreateEquipmentForm = function( Target, Sports ){

    let Form = $('<form>')
      .appendTo(Target);

      let NameGroup = $('<div>')
        .addClass('form-group row')
        .appendTo(Form);

      SportsGroup( Form, Sports );

      $('<label>')
        .addClass('col-sm-2 col-form-label')
        .attr('for', 'name')
        .html('Megnevezés')
        .appendTo(NameGroup);

      let NameGroupInputDiv = $('<div>')
        .addClass('col-sm-10')
        .appendTo(NameGroup);

      $('<input>')
        .addClass('form-control')
        .attr('type', 'text')
        .attr('name', 'name')
        .attr('placeholder', 'Megnevezés, minimum 3 karakter')
        .appendTo(NameGroupInputDiv);

    return Form;
}

Selectors.ReloadButton.click(function(){ LoadEquipments($(this)) });
Selectors.AddButton.click(function(){ AddEquipments() });
Selectors.Filter.Waste.click(function(){ Selectors.ReloadButton.trigger('click') })

$('body').on('click', '.equipment_editor', function(){ EquipmentEditor($(this)) })

Selectors.ReloadButton.trigger('click');
