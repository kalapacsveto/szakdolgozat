let login_form = $('form#form-signin');

login_form.submit(function(event){
  event.preventDefault();
  LoginRequest();
})


let LoginRequest = function(){
  let inputs = login_form.find(':input');
  let logindata = login_form.serializeArray();
  inputs.prop('disabled', true);

  $.ajax({
    type: "POST",
    url: '/login/request',
    data: logindata
  }).done(function(){
    location.reload(true);
  }).fail(function(Data){
    LoginFail();
    let inputs = login_form.find(':input');
    inputs.prop('disabled', false);
  });
}

let LoginFail = function(){
  let alert_container = $('#alert-container');
  alert_container.html('');
  let alert = $('<div>')
    .addClass('alert alert-danger alert-dismissible fade show')
    .attr('role', 'alert')
    .html('Hibás bejelentkezés')
    .appendTo(alert_container);

  let close_button = $('<button>')
    .attr('type', 'button')
    .addClass('close')
    .attr('data-dismiss', 'alert')
    .attr('aria-label', 'close')
    .html('<span aria-hidden="true">&times;</span>')
    .appendTo(alert);
}
