
export function GetSportsList(){
    return $.ajax({
      type: "GET",
      url: '/sports/list',
    });
}

export function GetEquipmentData(ID){
  return $.ajax({
    type: "GET",
    url: '/equipments/data',
    data: { id:ID }
  });
}

export function UpdateEquipment(FormData){
  return $.ajax({
    type: "POST", //PUT
    url: '/equipments/update',
    data: FormData
  });
}

export function AddEquipment(FormData){
  return $.ajax({
    type: "POST",
    url: '/equipments/add',
    data: FormData
  });
}

export function GetEquipments(Filter){
  return $.ajax({
    type: "GET",
    url: '/equipments/list',
    data : Filter
  });
}

export function GetUsers(){
  return $.ajax({
    type: "GET",
    url: '/users/list',
    data : {}
  });
}
