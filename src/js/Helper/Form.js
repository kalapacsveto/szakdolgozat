export function SportsGroup(Target, Sports){

  let SportsGroup = $('<div>')
    .addClass('form-group row')
    .appendTo(Target);

  $('<label>')
    .addClass('col-sm-2 col-form-label')
    .attr('for', 'sports')
    .html('Sportok')
    .appendTo(SportsGroup);


  let SportsCheckboxDiv = $('<div>')
    .addClass('col-sm-10 mt-2')
    .appendTo(SportsGroup);

  _.each(Sports, function(Sport){

    let InputGroup = $('<div>')
      .addClass('custom-control custom-checkbox custom-control-inline')
      .appendTo(SportsCheckboxDiv);

    $('<input>')
      .attr('type', 'checkbox')
      .addClass('custom-control-input')
      .attr('name','sports[]')
      .attr('id', 'CheckSport_'+Sport.id)
      .val(Sport.id)
      .appendTo(InputGroup);

    $('<label>')
      .addClass('custom-control-label')
      .attr('for', 'CheckSport_'+Sport.id)
      .html(Sport.name)
      .appendTo(InputGroup);

  });


  return SportsGroup;
}
