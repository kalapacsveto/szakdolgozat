import 'bootstrap';

export function CreateModal(Size, Backdrop){
  let Return = {};

  Return.Modal = $('<div>')
    .addClass('modal')
    .attr('tabindex', '-1')
    .appendTo('body');

  Return.Dialog = $('<div>')
    .addClass('modal-dialog')
    .appendTo(Return.Modal);

  Return.Content = $('<div>')
    .addClass('modal-content')
    .appendTo(Return.Dialog);

  Return.Header = $('<div>')
    .addClass('modal-header')
    .appendTo(Return.Content);

  Return.Body = $('<div>')
    .addClass('modal-body')
    .html('<div class="spinner-border" role="status"></div>')
    .appendTo(Return.Content);

  Return.Footer = $('<div>')
    .addClass('modal-footer')
    .appendTo(Return.Content);

  Return.Title = $('<h5>')
    .addClass('modal-title')
    .appendTo(Return.Header);

  $('<button>')
    .attr('type', 'button')
    .attr('data-dismiss', 'modal')
    .addClass('btn btn-secondary')
    .html('Mégse')
    .appendTo(Return.Footer);

  $('<button>')
    .addClass('close')
    .attr('type', 'button')
    .attr('data-dismiss', 'modal')
    .attr('aria-label', 'Close')
    .html('<span aria-hidden="true">&times;</span>')
    .appendTo(Return.Header);

  if(typeof Size !== 'undefined'){
    switch (Size) {
      case 'sm':
        Return.Dialog.addClass('modal-sm');
        break;
      case 'lg':
        Return.Dialog.addClass('modal-lg');
        break;
      case 'xl':
        Return.Dialog.addClass('modal-xl');
        break;
    }
  }

  if(typeof Backdrop !== 'undefined'){
    if(Backdrop === true){
      Return.Modal.addClass('fade');
      Return.Modal.attr('data-backdrop', 'static');
      Return.Modal.attr('data-keyboard', 'false');
    }
  }

  Return.Modal.on('hidden.bs.modal', function (e) { Return.Modal.remove() });

  return Return;
}
