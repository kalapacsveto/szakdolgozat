<div class="row">
  <div class="col-12 mb-2 text-right">
    <div class="btn-group btn-group-sm" role="group" aria-label="Funkciók">
      <div class="input-group-prepend">
        <div class="input-group-text">
          <span class="mr-2 small">törölt is</span>
          <input type="checkbox" aria-label="" name="filter_waste">
        </div>
      </div>
      <button id="equipments_reload" type="button" class="btn btn-secondary">frissítés</button>
      <button id="equipments_add" type="button" class="btn btn-primary">felvétel</button>
  </div>
  </div>
  <div class="col-12">
    <table id="equipments_list" class="table table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Megnevezés</th>
          <th scope="col">Tulajdonság</th>
          <th scope="col">Sportok</th>
          <th scope="col">Funkciók</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
