<div class="row">
  <div class="col-12">
    <h1 class="h3 mb-3 font-weight-normal">Bejelentkezés</h1>
    <label for="inputEmail" class="sr-only">Email cím</label>
    <form id="form-signin">
      <div id="alert-container"></div>
      <input type="email" name="email" id="inputEmail" class="form-control" placeholder="E-mail cím" required autofocus>
      <label for="inputPassword" class="sr-only">Jelszó</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Jelszó" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Bejelentkezés</button>
    </form>
  </div>
</div>
