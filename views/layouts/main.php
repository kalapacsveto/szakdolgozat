<!doctype html>
<html lang="hu">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $this->title ?></title>
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
        <a class="navbar-brand" href="/">Nyilvántartó</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="/users">Felhasználók</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/equipments">Eszközök</a>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bejelentkezve: <?php echo $_SESSION['user']['full_name'] ?></a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="login/logout">Kijelentkezés</a>
              </div>
            </li>
          </ul>
        </div>
        </div>
      </nav>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1><?= $this->title ?></h1>
        </div>
      </div>
    <?= $this->content ?>
    </div>
    <?= $this->js_list() ?>
  </body>
</html>
