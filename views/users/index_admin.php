<div class="row">
  <div class="col-12 mb-2 text-right">
    <div class="btn-group btn-group-sm" role="group" aria-label="Funkciók">
      <button id="users_reload" type="button" class="btn btn-secondary">frissítés</button>
      <button id="users_add" type="button" class="btn btn-primary">felvétel</button>
  </div>
  </div>
  <div class="col-12">
    <table id="users_list" class="table table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Név</th>
          <th scope="col">Email</th>
          <th scope="col">Típus</th>
          <th scope="col">Sportok</th>
          <th scope="col">Funkciók</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
